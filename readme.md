# Sample Notes Application

## Requirements

1. An index page for notes or to-do entries
2. A form for adding and editing existing entities
3. Storage of entries in a database

## Code

### Database Migrations
The application was developed on a MySQL database. Before running the migrations, create a new database and add the connection details to
the .env file.

The migrations are located in the migrations folder, to execute them run ```php artisan migrate```.

### Seeders

These are setup to add some sample data, you can run the seeders by ```php artisan db:seed ```.

### Controllers

There is only one controller ```NotesController.php```. 
This is a simple REST controller, most of the methods are accessed via AJAX. 

### Vue Components

#### app.js

This is the main Vue application. It handles the row selection and the actions for the buttons.
The delete is a simple AJAX call, the Edit and the New buttons change the application mode, so that the note editor is displayed.
It also catches when the editor is closed and restores the application mode to **initial** which will show the list of notes.

#### mb-note

This is the note editor component. It is only displayed when the application mode is **edit** or **store**.
It retrieves the fields for the selected row (via AJAX) and handles both editing existing and creating new notes.

### Validation

Validation is done server-side, using the StoreNote Form Request.

### Tests

PHPUnit tests can be found in the test folder. 

### Assets

Assets are compiled and versioned using GULP/Elixir. Details of this can be seen in ```gulpfile.js```.

## Screenshots
![picture](images/notes1.png)

![picture](images/notes2.png)




