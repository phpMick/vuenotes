<?php



    /*
    |--------------------------------------------------------------------------
    | Default Application Constants (MB not Taylor)
    |--------------------------------------------------------------------------
    | Usage: Config::get('application.name');
    |
    */

return [

    'feedURL' => 'http://feeds.feedburner.com/LinuxJournal-BreakingNews',
    'feedLimit' => 10 //seconds

];



