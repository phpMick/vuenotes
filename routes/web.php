<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//Resource controller for articles - try and REST as much as possible
Route::resource('notes', 'NotesController');

//Catch all to redirect to the homepage
Route::any('{query}',
    function () {
        return redirect('/notes');
    })
    ->where('query', '.*');