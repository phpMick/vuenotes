<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\Article;

class NotesTest extends TestCase
{

    use DatabaseMigrations;//migrates and rolls back


    /**
     * Check that the home page loads
     *
     */
    public function testHomePage()
    {

        $this->visit('/notes')
            ->see('Quick Notes');

    }


}
