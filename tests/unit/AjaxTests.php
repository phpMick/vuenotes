<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\Note;

class AjaxTests extends TestCase
{

    use DatabaseMigrations;//migrates and rolls back

    public function test_store_note()
    {
        //POST	    /photos	                store	photos.store

        $this->json('POST', '/notes', ['title' => 'Test Title','content' => 'Test Content'])
            ->seeJson([
                'status' => 'success',
            ]);



    }


    public function test_destroy_note()
    {
        //use a model factory to create a new model
        $note = factory(Note::class)->create();

        //first check it is there
        $this->seeInDatabase('notes',['id' => $note->id]);

        //call the delete route
        $response = $this->call('DELETE', '/notes/' .$note->id, ['_token' => csrf_token()]);

        //check that it has gone
        $this->dontSeeInDatabase('notes',['id' => $note->id]);

    }

    public function test_edit_note()
    {
        //GET	        /photos/{photo}/edit	edit	photos.edit

        //create note
        $note = factory(Note::class)->create();

        //test the edit route
        $this->json('GET', '/notes/' . $note->id . '/edit' )
            ->seeJson([
                'id' => $note->id,
            ]);



    }


    public function test_update_note()
    {
        //PUT/PATCH	/photos/{photo}	        update	photos.update

        //use a model factory to create a new model
        $note = factory(Note::class)->create();

        //change the title
        $this->json('PATCH', '/notes/' . $note->id, ['rowID'=>$note->id,'title'=>'Updated Title','content'=>'Updated Content','_token'=>csrf_token()] );


        //check that it has changed
        $changedNote = Note::find($note->id);

        $this->assertEquals('Updated Title',$changedNote->title);

    }



}
