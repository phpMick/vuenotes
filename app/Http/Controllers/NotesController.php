<?php

namespace App\Http\Controllers;

//Laravel


use App\Http\Requests\StoreNote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


//Models
use App\Models\Note;

/*
 * Todo:
 *
 * 1, An index page for notes or to-do entries
 * 2, A form for adding and editing existing entities
 * 3, Storage of entries in a database
 *
 * Migrations x
 * Seeds x
 * Tests x
 * Delete note x
 * Edit note x
 * New note x
 *
 * Disable buttons x
 * Row highlighting? x
 * Validation x
 * Readme
 *
 */
class NotesController extends Controller
{


    /**
     * Get all the notes.
     * Display the index view.
     *
     * @author Mick Byrne
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $notes = Note::all();

        return view('notes.list',compact('notes'));
    }


    /**
     *  AJAX call to delete a note.
     *
     * @author Mick Byrne
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $note = Note::findOrFail($id);

        $note->delete();

        return response()->json(['status'=>'success']);

    }


    /**
     * AJAX call to get the note details for editing.
     *
     * @author Mick Byrne
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {

        $note = Note::findOrFail($id);

        return response()->json(

            $note
        );

    }

    /**
     * AJAX call to save a new note.
     *
     * @author Mick Byrne
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreNote $request)
    {

        $note = new Note(
            ['title' => $request->title,
            'content' => $request->content]
        );

        $note->save();

        return response()->json(['status'=>'success']);

    }


    /**
     * AJAX call to update an existing note.
     *
     * @author Mick Byrne
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(StoreNote $request)
    {
        $note = Note::findOrFail($request->rowID);

        $note->title = $request->title;
        $note->content = $request->content;

        $note->save();

        return response()->json(['status'=>'success']);

    }

}
