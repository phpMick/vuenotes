<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Quick Notes Application</title>


	<link rel="stylesheet" href="{{ elixir('css/all.css') }}">

	<link rel="shortcut icon" href="/favicon.ico" >

	<meta name="csrf-token" content="{{ csrf_token() }}">

	<!-- Scripts -->
	<script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>




	</script>


	@yield('head')

</head>


<body class="cleared">


@yield('content')

<script src="{{ elixir('js/all.js') }}"></script>


<script src="{{ elixir('js/app.js') }}"></script>

<script>
	$.ajaxSetup({
		headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'}
	});

</script>




</body>
</html>














