@extends('layouts.app')



@section('content')

    <div class="container">
        <div id="app">


            <div class="jumbotron">
                <h1>Quick Notes</h1>
                <p>A Laravel/Vue.js Sample Application by Mick Byrne</p>
            </div>

            <div v-if="mode=='initial'">


                @include('notes.toolbar')


                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Notes</h3>
                    </div>
                    <div class="panel-body">

                        <table class="table table-bordered table-hover" id="notesTable">

                            @foreach ($notes as $note)
                                <tr class='clickable-row' v-bind:class="{ success: selectedRow == {{$note->id}} }">
                                    <td @click="rowClick({{$note->id}})">{{$note->title}} </td>

                                </tr>
                            @endforeach

                        </table>

                    </div>

                </div>
            </div>


            <div v-if="mode!='initial'">{{--show the note editor component--}}

                <mb-note v-on:close="editorClosed" v-bind:selected-row="selectedRow" v-bind:mode="mode">

                </mb-note>

            </div>

        </div> {{--app--}}
    </div> {{--container--}}

@endsection

