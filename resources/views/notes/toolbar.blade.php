<div class=" btn-toolbar">


    <button type="button" @click="btnDeleteClick" class="btn btn-danger"
            v-bind:class="{disabled: disableButton}">
        Delete
    </button>

    <button type="button" @click="btnEditClick" class="btn btn-primary"
            v-bind:class="{disabled: disableButton}">
        Edit
    </button>

    <button type="button" @click="btnNewClick" class="btn btn-success">
        New
    </button>

</div>

<br>