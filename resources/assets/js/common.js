/**
 *
 * Just to encapsulate the Sweet Alert
 *
 * @author Mick Byrne
 * @param text
 */
function sweetSuccess(text) {

    swal({
        title: 'Success',
        text: text,
        type: 'success',
        timer: 2000

    }).then(
        function () {},
        // handling the promise rejection
        function (dismiss) {

        }
    )
}//sweetSuccess


