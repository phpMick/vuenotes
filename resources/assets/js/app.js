
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('mb-note', require('./components/mb-note.vue'));



const app = new Vue({
    el: '#app',
    data: {
        selectedRow: null,
        mode: "initial",
        disableButton:true


    },

    methods: {
        /*
        * Tracks the selectedRow
        * Enables the Edit and Delete buttons
        */

        rowClick: function (rowClicked) {

            this.selectedRow =  rowClicked;

            if (this.selectedRow != null) {
                this.disableButton = false;
            }

        },

        btnDeleteClick: function () {

            //DELETE	    /photos/{photo}	        destroy	photos.destroy
            axios.delete('/notes/' + this.selectedRow)
                .then(function (response) {
                    sweetSuccess('Note Deleted');
                    //reload data
                    window.location.replace('/widgets')

                })
                .catch(function (error) {
                    console.log(error);
                });

        },

        //Changing the application displays the editor

        btnEditClick: function () {
            this.mode='edit';
        },

        btnNewClick: function () {
            this.mode='store';
        },

        editorClosed:function () {
          this.mode="initial";
        },
    }
});
